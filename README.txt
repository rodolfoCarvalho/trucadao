Para a executar a aplicação é necessário:


Ter instalado:
-Ruby
-Rails
-MySQL (BD usado no desenvolvimento)

Realizar os seguintes passos:
-Clonar o repositório git
-Entrar na pasta do repositório
-Configurar o User e Password do Banco de Dados em config/database.yml
-Executar no terminal
	$bundle install
	$rails db:create
	$rails db:migrate
	$rails s

A aplicação estará correndo normalmente sob o endereço:
http://localhost:3000