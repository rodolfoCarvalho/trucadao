class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :pd_type
      t.string :brand
      t.string :model
      t.integer :year
      t.float :price
      t.string :img
      t.integer :user_id

      t.timestamps
    end
  end
end
