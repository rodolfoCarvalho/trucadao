json.extract! product, :id, :pd_type, :brand, :model, :year, :price, :img, :created_at, :updated_at
json.url product_url(product, format: :json)
